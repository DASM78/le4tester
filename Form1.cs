﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Threading;
using System.Reflection;

namespace LE4Tester
{
    public partial class Form1 : Form
    {
        private System.IO.Ports.SerialPort m_serial = new SerialPort();
        private Thread m_thread;
        V92Measurements m_meas = new V92Measurements();
        V92CalibrRegs m_calibrationRegs = new V92CalibrRegs();
        V92Calibration m_calibration = new V92Calibration();
        sRealMeasurements m_realMeasurements = new sRealMeasurements();
        UInt32 m_sysreg = 0;

        const int SER_READ_TIMEOUT = 50;
        const int DEFAULT_PACKET_TIMEOUT = 500;
        const int DEFAULT_MAX_REPLY_PACKET_LEN = 256;

        public Form1()
        {
            InitializeComponent();
            string[] ports = SerialPort.GetPortNames();
            boxWrR.Text = "1000";
            boxRealU.Text = "235,0";
            boxRealI.Text = "5,00";
            boxRealP.Text = "1175,00";
            boxRealPQ.Text = "0";
            foreach (string port in ports) {
                boxComPort.Items.Add(port);
            }

            dataGridView1.ColumnCount = 2;
            dataGridView1.Columns[(int)0].HeaderText = "Reg";
            dataGridView1.Columns[(int)1].HeaderText = "Data";
            dataGridView1.RowCount = m_meas.rawLen / 4;

            /////////////////

            DataGridViewColumn c0 = new DataGridViewColumn();
            DataGridViewCell cell = new DataGridViewTextBoxCell();
            cell.Style.BackColor = Color.Wheat;
            c0.CellTemplate = cell;
            dataGridRegisters.Columns.Add(c0);
            DataGridViewTextBoxColumn c1 = new DataGridViewTextBoxColumn();
            dataGridRegisters.Columns.Add(c1);
            dataGridRegisters.RowCount = 25;
            int i = 0;
            foreach (var field in typeof(V92CalibrRegs).GetFields()) {
                var s = field.FieldType;
                string s1 = field.Name;
                if (!s1.StartsWith("dumm")) {
                    dataGridRegisters.Rows[i].Cells[0].Value = s1;
                    dataGridRegisters.Rows[i].Cells[0].ReadOnly = true;
                    i++;
                }
            }

            ///////////////////////////

            gridRealMeasurements.ColumnCount = 2;
            gridRealMeasurements.Columns[(int)0].HeaderText = "Reg";
            gridRealMeasurements.Columns[(int)1].HeaderText = "Data";
            gridRealMeasurements.RowCount = m_realMeasurements.rawLen / 4;

            m_thread = new Thread(threadFoo);
            timer1.Start();
        }

        private void boxComPort_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnConnect_Click(object sender, EventArgs e)
        {


            try {
                m_serial.PortName = boxComPort.SelectedItem.ToString();
                m_serial.Close();

                m_serial.BaudRate = 4800;
                m_serial.Parity = Parity.Even;
                m_serial.DataBits = 8;
                m_serial.StopBits = StopBits.One;
                m_serial.Encoding = Encoding.UTF8;
                m_serial.Open();
                lbPortState.Text = "Opened";
                if (!m_thread.IsAlive)
                    m_thread.Start();
            }
            catch {
                lbPortState.Text = "Closed";
                //MessageBox.Show("cant open");
            }
        }


        const int WR_ACCESS = 0x8000;
        enum Commands
        {
            //для добавления новых команд только дописывать в конец
            RD_DTIME = 0, READ_MEASUREMENTS = 200, READ_CALC_REGS, READ_SYSCTRL_REG, READ_REAL_MEASUREMENTS,
            WR_DTIME = 0 + WR_ACCESS, WRITE_CALC_REGS = 200 + WR_ACCESS, RESET_REQ, WRITE_SYSCTRL_REG, WRITE_CALIBRATION, WR_R
        };

        byte tocmd(Commands c)
        {
            return (byte)((byte)c & 0xff);
        }


        enum regsOffsets
        {
            OFS_FREQ = 4 * 0, ACT_POW = 4 * 1, REACT_POW = 4 * 2, IRMS = 4 * 3, URMS = 4 * 4,
            AVER_ACT = 4 * 5, AVER_REACT = 4 * 6, AVER_FREQ = 4 * 7, AVER_IRMS = 4 * 8, AVER_URMS = 4 * 9,
            UDC = 4 * 14, IDC = 4 * 15, ZXDATREG = 4 * 17, ZXDAT = 4 * 18, PHDAT = 4 * 19, T8BAUD = 4 * 21
        };

        bool bWrReg = false;
        bool bWrTime = false;
        bool fWrR = false;
        bool bWrCalibration = false;
        bool bRdReg = false;
        bool bResReq = false;

        bool isSummerTime () {
            return false;
        }

        public void threadFoo()
        {
            for (; ; ) {
                if (fWrR) {
                    int R = 0;
                    Int32.TryParse(boxWrR.Text, out R);                    
                    byte[] rep = sendPacketGetReply(COM.REC, tocmd(Commands.WR_R), 4, BitConverter.GetBytes(R));
                    if (rep != null)
                        fWrR = false;
                }

                if (bWrTime) {                    
                    CDateTime dt = new CDateTime(dtPicker.Value);
                    byte[] r = dt.asBCDRaw;
                    Array.Resize(ref r, r.Length + 2);
                    r[r.Length - 2] = 0; // rsvd
                    r[r.Length - 1] = isSummerTime() ? (byte)1 : (byte)0;
                    byte[] rep = sendPacketGetReply(COM.REC, tocmd(Commands.WR_DTIME), r.Length, r);
                    if (rep != null)
                        bWrTime = false;
                }
                if (bWrReg) {
                    bWrReg = false; //TODO change if write OK
                    byte[] rep = sendPacketGetReply(COM.REC, tocmd(Commands.WRITE_CALC_REGS), m_calibrationRegs.rawLen, m_calibrationRegs.asRaw);
                    byte[] repSys = sendPacketGetReply(COM.REC, tocmd(Commands.WRITE_SYSCTRL_REG), 4, BitConverter.GetBytes(m_sysreg)); // int as bytes
                }

                if (bWrCalibration) {
                    byte[] rep = sendPacketGetReply(COM.REC, tocmd(Commands.WRITE_CALIBRATION), m_calibration.rawLen, m_calibration.asRaw);
                    if (rep != null)
                        bWrCalibration = false;
                }


                if (bResReq) {
                    byte[] rep = sendPacketGetReply(COM.REC, tocmd(Commands.RESET_REQ));
                    if (rep != null)
                        bResReq = false;
                }

                if (bRdReg) {

                    byte[] rep = sendPacketGetReply(COM.ENQ, tocmd(Commands.READ_CALC_REGS), MAX_REPLYLEN, null, 3000); Thread.Sleep(10);
                //    byte[] repsys = sendPacketGetReply(COM.ENQ, tocmd(Commands.READ_SYSCTRL_REG), MAX_REPLYLEN, null, 5000);
                    if (rep != null && rep.Length != 0) {

                        bRdReg = false;
                        isRegReady = true;
                        m_calibrationRegs.asRaw = rep;
                        //m_sysreg = BitConverter.ToUInt32(repsys, 0);
                    }
                }
                byte[] rawMeas = sendPacketGetReply(COM.ENQ, 0xc8, m_meas.rawLen);
                if (rawMeas == null) continue;
                if (rawMeas.Length != m_meas.rawLen) continue;
                m_meas.asRaw = rawMeas;
                Thread.Sleep(80);
                byte[] realMeas = sendPacketGetReply(COM.ENQ, 0x40, m_realMeasurements.rawLen);
                if (realMeas == null) continue;
                if (realMeas.Length != m_realMeasurements.rawLen) continue;
                m_realMeasurements.asRaw = realMeas;
                Thread.Sleep(120);
                isDataReady = true;
            }

        }


        const int START_MARK = 0x02;
        private void sendPacket(byte[] data)
        {
            try {
                if (data.Length > 255 - 2 - 2)
                    return;
                byte dataLen = (byte)data.Length;
                byte[] tosend = new byte[dataLen + 2 + 2];
                tosend[0] = START_MARK;
                tosend[1] = (byte)(dataLen + 2 + 2);
                Array.Copy(data, 0, tosend, 2, dataLen);
                ushort crc = crc16.crc16calc(tosend, 0, tosend.Length - 2);
                tosend[tosend.Length - 2] = (byte)crc;
                tosend[tosend.Length - 1] = (byte)(crc >> 8);
                m_serial.Write(tosend, 0, tosend.Length);
            }
            catch {
                MessageBox.Show("port closed");
                throw;
            }
        }


        // чтение на низком уровне, читает все, если получено меньше, чем ожидали - вернет меньший размер
        private byte[] readTimeOut(int count, int timeout)
        {
            byte[] buffer = new byte[count];
            m_serial.ReadTimeout = timeout;
            int ofs = 0;
            try {
                while (ofs < count) {
                    buffer[ofs] = (byte)(m_serial.ReadByte());
                    ofs++;
                }
            }
            catch (TimeoutException e) {

            }
            catch {

            }

            Array.Resize(ref buffer, ofs);
            return buffer;
        }



        public byte[] readReply(int expectBodyLen, int timeout = DEFAULT_PACKET_TIMEOUT)
        {
            long timeEnd = DateTime.Now.Ticks + TimeSpan.TicksPerMillisecond * timeout;
            byte[] reply = null;
            do {
                reply = readPacketBody(expectBodyLen, SER_READ_TIMEOUT);
                if (reply != null)
                    break;
            } while (DateTime.Now.Ticks <= timeEnd);
            return reply;
        }

        //вернет массив с телом ответа (без фильтрации по по сетевому адресу
        // параметр - ожидаемая длина тела ответа
        // если принятный пакет содержит меньшую длину, чем ожидаемую - вернет массив с реальной длиной ответа (может быть меньше
        // запрашиваемой. Если равную или большую - усечет до размеров ожидаемой длины
        // таймаут тут - междубайтный, для разделения пакетов . 
        private byte[] readPacketBody(int expectBodyLen, int timeout)
        {
            byte[] body = new byte[expectBodyLen];
            int infoLen = 1 + 1 + 4 + 4 + 1 + 1 + 2;
            int bodyOffset = infoLen - 2; // except crc
            int fullLen = expectBodyLen + infoLen;
            byte[] rawReply = readTimeOut(fullLen, timeout); // full packet

            int realBodyLen = 0;
            if (rawReply.Length < infoLen)
                return null;
            realBodyLen = rawReply.Length - infoLen;
            if (realBodyLen > expectBodyLen)
                realBodyLen = expectBodyLen;
            Array.Copy(rawReply, bodyOffset, body, 0, realBodyLen);
            Array.Resize(ref body, realBodyLen);
            return body;
        }


        class netAddress
        {
            public netAddress(UInt32 a)
            {
                addr = new byte[4];
                addr[0] = (byte)a;
                addr[1] = (byte)(a >> 8);
                addr[2] = (byte)(a >> 16);
                addr[3] = (byte)(a >> 24);
            }
            public byte[] addr { set; get; }
        }
        class password
        {
            public password(UInt32 a)
            {
                raw = new byte[4];
                raw[0] = (byte)a;
                raw[1] = (byte)(a >> 8);
                raw[2] = (byte)(a >> 16);
                raw[3] = (byte)(a >> 24);
            }
            public byte[] raw { set; get; }
        }
        enum COM { ENQ = 1, REC = 3, DRJ = 0x0a };



        private UInt32 defaultPassword = 0x0001b207;
        private UInt32 defaultAddr = 0x0;
        private void sendPacket(COM com, byte id, byte[] data = null, netAddress a = null, password p = null)
        {
            const int offs_data = 10;
            byte[] b = new byte[256];
            if (a == null)
                a = new netAddress(defaultAddr);
            Array.Copy(a.addr, b, a.addr.Length);
            if (p == null)
                p = new password(defaultPassword);
            Array.Copy(p.raw, 0, b, a.addr.Length, p.raw.Length);
            b[8] = (byte)com;
            b[9] = (byte)id;
            if (data != null)
                Array.Copy(data, 0, b, offs_data, data.Length); //
            if (data != null)
                Array.Resize(ref b, data.Length + offs_data);
            else
                Array.Resize(ref b, 0 + offs_data);
            sendPacket(b);
        }

        const int MAX_REPLYLEN = 255;
        private byte[] sendPacketGetReply(COM com, byte id, int replyLen = MAX_REPLYLEN, byte[] packetData = null, int timeout = DEFAULT_PACKET_TIMEOUT, netAddress a = null, password p = null)
        {
            sendPacket(com, id, packetData, a, p);
            byte[] bd = readReply(replyLen, timeout);
            return bd;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {

            m_thread.Start();
        }



        class regDesc
        {
            public regDesc(string desc)
            {
                m_desc = desc;
            }
            public int val;
            public string m_desc;
        };


        class V92CalibrRegs
        {
            public Int32 pac;
            public Int32 phc;
            public Int32 padcc;
            public Int32 qac;
            public Int32 dummy1;
            public Int32 qadcc;
            public Int32 dummy2;
            public Int32 iac;
            public Int32 iadcc;
            public Int32 uc;
            public Int32 dummy3;
            public Int32 dummy4;
            public Int32 dummy5;
            public Int32 dummy6;
            public Int32 iaadcc;
            public Int32 dummy7;
            public Int32 uadcc;
            public Int32 bpfpara;
            public Int32 udcc;
            public Int32 chcksum;
            public int rawLen { get { return 20 * 4; } }
            public byte[] asRaw {
                get {
                    byte[] raw = new byte[rawLen];
                    int dst = 0;
                    Array.Copy(BitConverter.GetBytes(pac), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(phc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(padcc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(qac), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(dummy1), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(qadcc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(dummy2), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(iac), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(iadcc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(uc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(dummy3), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(dummy4), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(dummy5), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(dummy6), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(iaadcc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(dummy7), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(uadcc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(bpfpara), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(udcc), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(chcksum), 0, raw, dst, 4); dst += 4;
                    return raw;
                }
                set {
                    int idx = 0;
                    pac = BitConverter.ToInt32(value, idx); idx += 4;
                    phc = BitConverter.ToInt32(value, idx); idx += 4;
                    padcc = BitConverter.ToInt32(value, idx); idx += 4;
                    qac = BitConverter.ToInt32(value, idx); idx += 4;
                    dummy1 = BitConverter.ToInt32(value, idx); idx += 4;
                    qadcc = BitConverter.ToInt32(value, idx); idx += 4;
                    dummy2 = BitConverter.ToInt32(value, idx); idx += 4;

                    iac = BitConverter.ToInt32(value, idx); idx += 4;
                    iadcc = BitConverter.ToInt32(value, idx); idx += 4;
                    uc = BitConverter.ToInt32(value, idx); idx += 4;
                    dummy3 = BitConverter.ToInt32(value, idx); idx += 4;
                    dummy4 = BitConverter.ToInt32(value, idx); idx += 4;
                    dummy5 = BitConverter.ToInt32(value, idx); idx += 4;
                    dummy6 = BitConverter.ToInt32(value, idx); idx += 4;

                    iaadcc = BitConverter.ToInt32(value, idx); idx += 4;
                    dummy7 = BitConverter.ToInt32(value, idx); idx += 4;
                    uadcc = BitConverter.ToInt32(value, idx); idx += 4;
                    bpfpara = BitConverter.ToInt32(value, idx); idx += 4;
                    udcc = BitConverter.ToInt32(value, idx); idx += 4;
                    chcksum = BitConverter.ToInt32(value, idx); idx += 4;
                }
            }
        }


        static class BCD
        {
            static public byte [] bin2bcd(byte[] n) {
                byte [] bb = new byte[n.Length];
                for (int i = 0; i < bb.Length; i++) {
                    bb[i] = bin2bcd(n[i]);
                }
                return bb;
            }
            static public byte bin2bcd(byte n)   {
                return (byte)((((n) / 10) << 4) + ((n) % 10) );
            }
            static public byte bin2bcd(int n)   {
                return bin2bcd((byte) n);
            }

            static public byte bcd2bin(byte n)   {
                return (byte)(((n) >> 4) * 10 + ((n) & 0x0F));
            }
            static public byte bcd2bin(int n)   {
                return bcd2bin((byte)n);
            }

        }

        class CDateTime
        {
            public CDateTime(DateTime dt)
            {
                m_dt = dt;
            }
            public DateTime dt {
                set {
                    m_dt = value;
                }
                get {
                    return m_dt;
                }
            }
            public int rawLen { get { return 7; } }
            public byte[] asBCDRaw {
                set {
                    int s = BCD.bcd2bin(value[0]);
                    int m = BCD.bcd2bin(value[1]);
                    int h = BCD.bcd2bin(value[2]);
                    int dm = BCD.bcd2bin(value[3]);
                    int mn = BCD.bcd2bin(value[4]);
                    int y = BCD.bcd2bin(value[5]);                    
                    DateTime newDT = new DateTime(y + 1900, mn , dm, h, m, s);
                    m_dt = newDT;                    
                }
                get {
                    byte[] raw = new byte[rawLen];
                    int dst = 0;
                    raw[dst] = BCD.bin2bcd(m_dt.Second); dst++;
                    raw[dst] = BCD.bin2bcd(m_dt.Minute); dst++;
                    raw[dst] = BCD.bin2bcd(m_dt.Hour); dst++;
                    raw[dst] = BCD.bin2bcd(m_dt.Day); dst++;
                    raw[dst] = BCD.bin2bcd(m_dt.Month - 1); dst++;
                    raw[dst] = BCD.bin2bcd(m_dt.Year - 1900); dst++;
                    raw[dst] = BCD.bin2bcd((byte)m_dt.DayOfWeek); 
                    // Array.Copy(BitConverter.GetBytes(m_dt.Hour), 0, raw, dst, 4); dst += 1;
                    return raw;
                }
            }
            private DateTime m_dt;
        }

        class V92Calibration
        {
            public float kU;
            public float kI;
            public float kP;
            public float kPQ;
            public int rawLen { get { return 4 * 4; } }
            public byte[] asRaw {
                set {

                }
                get {
                    byte[] raw = new byte[rawLen];
                    int dst = 0;
                    Array.Copy(BitConverter.GetBytes(kU), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(kI), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(kP), 0, raw, dst, 4); dst += 4;
                    Array.Copy(BitConverter.GetBytes(kPQ), 0, raw, dst, 4); dst += 4;
                    return raw;
                }
            }

        }

        class V92Measurements
        {
            public int FREQINST;
            public int PAINST;
            public int QINST;
            public int IAINST;
            public int UINST;
            public int PAAVG;
            public int QAVG;
            public int FREQAVG;
            public int IAAVG;
            public int UAVG; //UAVG d4
            public int x1 = 0; //d5
            public int x2 = 0; //d6
            public int x3 = 0; //d7
            public int x4 = 0; //d8
            public int UDCINST;
            public int IADCINST;
            public int x5 = 0; //db
            public int ZXDATREG;
            public int ZXDAT;
            public int PHDAT;
            public int x6 = 0; //df
            public int T8BAUD;


            public int rawLen { get { return 22 * 4; } }
            public byte[] asRaw {
                set {
                    FREQINST = BitConverter.ToInt32(value, (int)regsOffsets.OFS_FREQ);
                    PAINST = BitConverter.ToInt32(value, (int)regsOffsets.ACT_POW);
                    QINST = BitConverter.ToInt32(value, (int)regsOffsets.REACT_POW);
                    IAINST = BitConverter.ToInt32(value, (int)regsOffsets.IRMS);
                    UINST = BitConverter.ToInt32(value, (int)regsOffsets.URMS);
                    PAAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_ACT);
                    QAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_REACT);
                    FREQAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_FREQ);
                    IAAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_IRMS);
                    UAVG = BitConverter.ToInt32(value, (int)regsOffsets.AVER_URMS);
                    UDCINST = BitConverter.ToInt32(value, (int)regsOffsets.UDC);
                    IADCINST = BitConverter.ToInt32(value, (int)regsOffsets.IDC);
                    ZXDATREG = BitConverter.ToInt32(value, (int)regsOffsets.ZXDATREG);
                    ZXDAT = BitConverter.ToInt32(value, (int)regsOffsets.ZXDAT);
                    PHDAT = BitConverter.ToInt32(value, (int)regsOffsets.PHDAT);
                    T8BAUD = BitConverter.ToInt32(value, (int)regsOffsets.T8BAUD);
                }
            }
        }




        class sRealMeasurements
        {
            public float curU;
            public float curI;
            public float curP;
            public float curQ;
            public float curEnergy;
            public float curQEnergy;
            public int rawLen { get { return 6 * 4; } }
            public byte[] asRaw {
                set {
                    int src = 0;
                    curU = BitConverter.ToSingle(value, src); src += 4;
                    curI = BitConverter.ToSingle(value, src); src += 4;
                    curP = BitConverter.ToSingle(value, src); src += 4;
                    curQ = BitConverter.ToSingle(value, src); src += 4;
                    curEnergy = BitConverter.ToSingle(value, src); src += 4;
                    curQEnergy = BitConverter.ToSingle(value, src); src += 4;
                }
            }

        }




        class crc16
        {
            static public ushort crc16calc(byte[] b)
            {
                return crc16calc(b, 0, b.Length);
            }
            static public ushort crc16calc(byte[] arr, int offest, int length)
            {
                if (offest >= length) throw new Exception();
                byte[] b = new byte[length - offest];
                Array.Copy(arr, offest, b, 0, length);
                byte d = 0;
                int idx = 0;
                int len = b.Length;
                ushort crc = 0xffff;
                do {
                    d = (byte)(b[idx++] ^ (byte)crc);
                    d ^= (byte)(d << 4);
                    crc = (ushort)((d << 3) ^ (d << 8) ^ (crc >> 8) ^ (d >> 4));
                }
                while (--len > 0);
                crc ^= 0xffff;
                return crc;
            }

        }

        bool isDataReady = false;
        bool isRegReady = false;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (isDataReady) {

                FieldInfo[] fields = typeof(V92Measurements).GetFields();
                for (int i = 0; i < fields.Length; i++) {
                    dataGridView1.Rows[i].Cells[0].Value = fields[i].Name;
                    dataGridView1.Rows[i].Cells[1].Value = fields[i].GetValue(m_meas);
                    //  dataGridView1.Rows[i].Cells[2].Value =// fields[i].Name;
                    //    fields[i].SetValue(m_calibrationRegs, getValFromTableByName(fields[i].Name));
                }


                FieldInfo[] fieldsReal = typeof(sRealMeasurements).GetFields();
                for (int i = 0; i < fieldsReal.Length; i++) {
                    gridRealMeasurements.Rows[i].Cells[0].Value = fieldsReal[i].Name;
                    gridRealMeasurements.Rows[i].Cells[1].Value = fieldsReal[i].GetValue(m_realMeasurements);
                }


                isDataReady = false;
            }
            if (isRegReady) {
                FieldInfo[] fields = typeof(V92CalibrRegs).GetFields();
                for (int i = 0; i < fields.Length; i++) {
                    dataGridRegisters.Rows[i].Cells[0].Value = fields[i].Name;
                    dataGridRegisters.Rows[i].Cells[1].Value = fields[i].GetValue(m_calibrationRegs);
                }
                boxSys.Text = m_sysreg.ToString("X");
                isRegReady = false;
            }


        }


        private DataGridViewRow getRowByName(DataGridView view, string name)
        {
            foreach (DataGridViewRow r in view.Rows) {
                var s = r.Cells[0].Value;
                if ((string)s == name) {
                    return r;
                }
            }
            return null;
        }

        private void button1_Click(object sender, EventArgs e)
        {

            int getValFromTableByName(string name)
            {
                Int32 result = 0;
                try {
                    // прикольно, язык не разрешает объявлять переменную в if как в сях
                    if (getRowByName(dataGridRegisters, name) is var r && r != null) {
                        Int32.TryParse(r.Cells[1].Value.ToString(), out result);
                    }
                    return result;
                }
                catch {
                    MessageBox.Show("Invalid value");
                    return 0;
                }
            }

            DataGridViewColumn coll = dataGridRegisters.Columns[0];
            try {
                FieldInfo[] fields = typeof(V92CalibrRegs).GetFields();
                for (int i = 0; i < fields.Length; i++) {
                    fields[i].SetValue(m_calibrationRegs, getValFromTableByName(fields[i].Name));
                }

                m_sysreg = UInt32.Parse(boxSys.Text, System.Globalization.NumberStyles.HexNumber);  // 16 parse hex              
                bWrReg = true;
            }
            catch {

            }
        }

        private void btnRd_Click(object sender, EventArgs e)
        {
            bRdReg = true;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            bResReq = true;
        }

        private void btnCalibrate_Click(object sender, EventArgs e)
        {
            double realU = 0, realI = 0, realP = 0, realPQ = 0;
            try {

                Double.TryParse(boxRealU.Text, out realU);
                Double.TryParse(boxRealI.Text, out realI);
                Double.TryParse(boxRealP.Text, out realP);
                Double.TryParse(boxRealPQ.Text, out realPQ);

                V92Calibration temp = new V92Calibration();
                temp.kU = temp.kI = temp.kP = temp.kPQ = 0;
                if (m_meas.UAVG != 0)
                    temp.kU = (float)realU / m_meas.UAVG;
                if (m_meas.IAAVG != 0)
                    temp.kI = (float)realI / m_meas.IAAVG;
                if (m_meas.PAAVG != 0)
                    temp.kP = (float)realP / m_meas.PAAVG;
                if (m_meas.QAVG != 0)
                    temp.kPQ = (float)realPQ / m_meas.QAVG;

                m_calibration = temp;
                bWrCalibration = true;

            }
            catch {
                MessageBox.Show("Корявые данные ");
            }

        }

        private void dataGridRegisters_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnTime_Click(object sender, EventArgs e)
        {
            bWrTime = true;
        }

        private void bWrR_Click(object sender, EventArgs e)
        {
            fWrR = true;
        }

        private void dtPicker_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void dtPicker_DropDown(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}