﻿namespace LE4Tester
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.boxComPort = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnConnect = new System.Windows.Forms.Button();
            this.lbPortState = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataSet1 = new System.Data.DataSet();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridRegisters = new System.Windows.Forms.DataGridView();
            this.btnRd = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.boxSys = new System.Windows.Forms.TextBox();
            this.boxRealU = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.boxRealPQ = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.boxRealP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnCalibrate = new System.Windows.Forms.Button();
            this.boxRealI = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.gridRealMeasurements = new System.Windows.Forms.DataGridView();
            this.dtPicker = new System.Windows.Forms.DateTimePicker();
            this.btnTime = new System.Windows.Forms.Button();
            this.boxWrR = new System.Windows.Forms.TextBox();
            this.bWrR = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisters)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridRealMeasurements)).BeginInit();
            this.SuspendLayout();
            // 
            // boxComPort
            // 
            this.boxComPort.FormattingEnabled = true;
            this.boxComPort.Location = new System.Drawing.Point(1069, 469);
            this.boxComPort.Name = "boxComPort";
            this.boxComPort.Size = new System.Drawing.Size(121, 21);
            this.boxComPort.TabIndex = 0;
            this.boxComPort.SelectedIndexChanged += new System.EventHandler(this.boxComPort_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1085, 448);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Port";
            // 
            // btnConnect
            // 
            this.btnConnect.Location = new System.Drawing.Point(1115, 540);
            this.btnConnect.Name = "btnConnect";
            this.btnConnect.Size = new System.Drawing.Size(75, 23);
            this.btnConnect.TabIndex = 2;
            this.btnConnect.Text = "Connect";
            this.btnConnect.UseVisualStyleBackColor = true;
            this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
            // 
            // lbPortState
            // 
            this.lbPortState.AutoSize = true;
            this.lbPortState.Location = new System.Drawing.Point(1151, 504);
            this.lbPortState.Name = "lbPortState";
            this.lbPortState.Size = new System.Drawing.Size(39, 13);
            this.lbPortState.TabIndex = 3;
            this.lbPortState.Text = "Closed";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(26, 71);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(282, 496);
            this.dataGridView1.TabIndex = 5;
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            // 
            // timer1
            // 
            this.timer1.Interval = 25;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(433, 26);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Wr";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridRegisters
            // 
            this.dataGridRegisters.AllowUserToAddRows = false;
            this.dataGridRegisters.AllowUserToDeleteRows = false;
            this.dataGridRegisters.AllowUserToOrderColumns = true;
            this.dataGridRegisters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridRegisters.Location = new System.Drawing.Point(321, 71);
            this.dataGridRegisters.Name = "dataGridRegisters";
            this.dataGridRegisters.Size = new System.Drawing.Size(279, 496);
            this.dataGridRegisters.TabIndex = 8;
            this.dataGridRegisters.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridRegisters_CellContentClick);
            // 
            // btnRd
            // 
            this.btnRd.Location = new System.Drawing.Point(321, 26);
            this.btnRd.Name = "btnRd";
            this.btnRd.Size = new System.Drawing.Size(75, 23);
            this.btnRd.TabIndex = 9;
            this.btnRd.Text = "Rd";
            this.btnRd.UseVisualStyleBackColor = true;
            this.btnRd.Click += new System.EventHandler(this.btnRd_Click);
            // 
            // btnReset
            // 
            this.btnReset.Location = new System.Drawing.Point(563, 26);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 10;
            this.btnReset.Text = "SysRes";
            this.btnReset.UseVisualStyleBackColor = true;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // boxSys
            // 
            this.boxSys.Location = new System.Drawing.Point(26, 29);
            this.boxSys.Name = "boxSys";
            this.boxSys.Size = new System.Drawing.Size(100, 20);
            this.boxSys.TabIndex = 11;
            // 
            // boxRealU
            // 
            this.boxRealU.Location = new System.Drawing.Point(20, 39);
            this.boxRealU.Name = "boxRealU";
            this.boxRealU.Size = new System.Drawing.Size(100, 20);
            this.boxRealU.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.boxRealPQ);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.boxRealP);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.btnCalibrate);
            this.groupBox1.Controls.Add(this.boxRealI);
            this.groupBox1.Controls.Add(this.boxRealU);
            this.groupBox1.Location = new System.Drawing.Point(911, 71);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(270, 306);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 186);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 20;
            this.label5.Text = "Реальная PQ";
            // 
            // boxRealPQ
            // 
            this.boxRealPQ.Location = new System.Drawing.Point(20, 179);
            this.boxRealPQ.Name = "boxRealPQ";
            this.boxRealPQ.Size = new System.Drawing.Size(100, 20);
            this.boxRealPQ.TabIndex = 19;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(156, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Реальная P";
            // 
            // boxRealP
            // 
            this.boxRealP.Location = new System.Drawing.Point(20, 141);
            this.boxRealP.Name = "boxRealP";
            this.boxRealP.Size = new System.Drawing.Size(100, 20);
            this.boxRealP.TabIndex = 17;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(160, 85);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Реальное I";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(160, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Реальное U";
            // 
            // btnCalibrate
            // 
            this.btnCalibrate.Location = new System.Drawing.Point(20, 260);
            this.btnCalibrate.Name = "btnCalibrate";
            this.btnCalibrate.Size = new System.Drawing.Size(100, 23);
            this.btnCalibrate.TabIndex = 14;
            this.btnCalibrate.Text = "Калибровка";
            this.btnCalibrate.UseVisualStyleBackColor = true;
            this.btnCalibrate.Click += new System.EventHandler(this.btnCalibrate_Click);
            // 
            // boxRealI
            // 
            this.boxRealI.Location = new System.Drawing.Point(20, 78);
            this.boxRealI.Name = "boxRealI";
            this.boxRealI.Size = new System.Drawing.Size(100, 20);
            this.boxRealI.TabIndex = 13;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.gridRealMeasurements);
            this.groupBox2.Location = new System.Drawing.Point(616, 71);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(271, 264);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBox2";
            // 
            // gridRealMeasurements
            // 
            this.gridRealMeasurements.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridRealMeasurements.Location = new System.Drawing.Point(7, 45);
            this.gridRealMeasurements.Name = "gridRealMeasurements";
            this.gridRealMeasurements.Size = new System.Drawing.Size(248, 186);
            this.gridRealMeasurements.TabIndex = 0;
            // 
            // dtPicker
            // 
            this.dtPicker.Location = new System.Drawing.Point(651, 543);
            this.dtPicker.Name = "dtPicker";
            this.dtPicker.Size = new System.Drawing.Size(200, 20);
            this.dtPicker.TabIndex = 15;
            this.dtPicker.ValueChanged += new System.EventHandler(this.dtPicker_ValueChanged);
            this.dtPicker.DropDown += new System.EventHandler(this.dtPicker_DropDown);
            // 
            // btnTime
            // 
            this.btnTime.Location = new System.Drawing.Point(911, 543);
            this.btnTime.Name = "btnTime";
            this.btnTime.Size = new System.Drawing.Size(75, 23);
            this.btnTime.TabIndex = 16;
            this.btnTime.Text = "Time";
            this.btnTime.UseVisualStyleBackColor = true;
            this.btnTime.Click += new System.EventHandler(this.btnTime_Click);
            // 
            // boxWrR
            // 
            this.boxWrR.Location = new System.Drawing.Point(651, 469);
            this.boxWrR.Name = "boxWrR";
            this.boxWrR.Size = new System.Drawing.Size(100, 20);
            this.boxWrR.TabIndex = 17;
            // 
            // bWrR
            // 
            this.bWrR.Location = new System.Drawing.Point(776, 467);
            this.bWrR.Name = "bWrR";
            this.bWrR.Size = new System.Drawing.Size(75, 23);
            this.bWrR.TabIndex = 18;
            this.bWrR.Text = "Зап";
            this.bWrR.UseVisualStyleBackColor = true;
            this.bWrR.Click += new System.EventHandler(this.bWrR_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(651, 448);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Р счетчика";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1209, 615);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.bWrR);
            this.Controls.Add(this.boxWrR);
            this.Controls.Add(this.btnTime);
            this.Controls.Add(this.dtPicker);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.boxSys);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnRd);
            this.Controls.Add(this.dataGridRegisters);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.lbPortState);
            this.Controls.Add(this.btnConnect);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.boxComPort);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridRegisters)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridRealMeasurements)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox boxComPort;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnConnect;
        private System.Windows.Forms.Label lbPortState;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Data.DataSet dataSet1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridRegisters;
        private System.Windows.Forms.Button btnRd;
        private System.Windows.Forms.Button btnReset;
        private System.Windows.Forms.TextBox boxSys;
        private System.Windows.Forms.TextBox boxRealU;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox boxRealP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnCalibrate;
        private System.Windows.Forms.TextBox boxRealI;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox boxRealPQ;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView gridRealMeasurements;
        private System.Windows.Forms.DateTimePicker dtPicker;
        private System.Windows.Forms.Button btnTime;
        private System.Windows.Forms.TextBox boxWrR;
        private System.Windows.Forms.Button bWrR;
        private System.Windows.Forms.Label label6;
    }
}

